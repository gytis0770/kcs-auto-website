 <?php 
 	require('../db/session.php');

 	if($_SESSION['userType'] !== 0) {
		$_SESSION['error'] = 'Jūs neturite teisių matyti šio turinio!';
		header('Location: ../error.php');
	}
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Vartotojo panelė</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	

	<div class="container">
		<?php

			if(!empty($_SESSION['isLoggedIn'])) :
				if($_SESSION['isLoggedIn']) : ?>

				<a href="../db/logout.php" class="btn btn-info">
					Atsijungti
				</a>

				<?php
				endif;
			endif;

		?>
		<h1>Sveiki atvyke, 
			<span>
				<?php echo $user['fullname']; ?>
			</span>
		</h1>

		<ul>
			<li><a href="auto/create/createView.php">Pridėti skelbimą</a></li>
			<li><a href="auto/list/listView.php">Peržiūrėti skelbimus</a></li>
		</ul>
	</div>

</body>
</html>