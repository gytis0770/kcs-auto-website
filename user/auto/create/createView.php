<?php
	
	session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AutoKCS</title>
	
	<!-- Bootstrap nuoroda -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Musu stilius -->
	<link rel="stylesheet" type="text/css" href="./assets/css/custom.css">
</head>
<body>

	<div class="container">
		<h1>Pridėti naują skelbimą</h1>

		<form action="create.php" method="POST">
			<div class="form-group">
				<label>Markė:</label>
				<input class="form-control" type="text" name="brand">
			</div>
			<div class="form-group">
				<label>Modelis:</label>
				<input class="form-control" type="text" name="model">
			</div>
			<div class="form-group">
				<label>Metai:</label>
				<input class="form-control" type="date" name="year">
			</div>

			<div class="form-group">
				<label>Kėbulo tipas:</label>
				<input class="form-control" type="text" name="type">
			</div>

			<div class="form-group">
				<label>Variklio tūris:</label>
				<input class="form-control" type="text" name="engine">
			</div>

			<input type="hidden" name="user_id" value="<?php echo $_SESSION['userId']; ?>">

			<div class="form-group">
				<button class="btn btn-warning">Pridėti</button>
			</div>
		</form>
	</div>


</body>
</html>



