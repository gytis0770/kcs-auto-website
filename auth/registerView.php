<?php
	
	session_start();

	if(!empty($_SESSION['isLoggedIn'])) {
		if($_SESSION['isLoggedIn'] && $_SESSION['userType'] === 0) {
		header('Location: ../user/user.php');
		}

		if($_SESSION['isLoggedIn'] && $_SESSION['userType'] === 1) {
			header('Location: ../admin/admin.php');
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AutoKCS - registracija</title>
	
	<!-- Bootstrap nuoroda -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Musu stilius -->
	<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
</head>
<body>

	<div class="container">
		
			<form action="register.php" method="POST">
				<div class="form-group">
					<label>Vartotojo vardas</label>
					<input type="text" name="username" class="form-control" required>
				</div>
				<div class="form-group">
					<label>El. paštas</label>
					<input type="email" name="email" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Slaptažodis</label>
					<input type="password" name="password" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Patvirtinti slaptažodį</label>
					<input type="password" name="cpassword" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Vardas Pavardė</label>
					<input type="text" name="fullname" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Miestas</label>
					<input type="text" name="city" class="form-control" required>
				</div>


				<div class="form-group">
					<button class="btn btn-info">Registracija</button>
				</div>
			</form>


	</div>

</body>
</html>



