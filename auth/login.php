<?php
	
	session_start();

	require('../db/connection.php');
	/*
		Prisijungimas:
		tikriname ar email ir password laukeliai uzpildyti.
		Jei taip, surandame vartotojo pagal ivesta email, slaptazodi
		ir lyginame su katik ivestu formoje (db === formos)
	 */
	if(!empty($_POST['email']) && !empty($_POST['password']))
	{
		// Gaunam duomenis is formos
		$email 		= $_POST['email'];
		$password 	= $_POST['password'];

		try {

			$stmt = $conn->prepare("SELECT * FROM users WHERE email='$email'");
			$stmt->execute();

			$result = $stmt->fetch();
			$dbpassword = $result['password'];
			$dbtype = (int)$result['type'];
			$dbuser = (int)$result['id'];

		} catch(PDOException $e) {
			echo "Klaida: " . $e->getMessage();
		}

		// Tikrinimas ar slaptazodziai sutampa
		if(password_verify($password, $dbpassword)) {

			// Vartotojas prisijunges
			// Vartotojo id
			// Vartotojo tipas

			$_SESSION['isLoggedIn'] = true;
			$_SESSION['userId'] = $dbuser;
			$_SESSION['userType'] = $dbtype;

			// Nukreipimas pagal vartotojo tipa
			if($dbtype === 0) {
				header('Location: ../user/user.php');
			} else {
				header('Location: ../admin/admin.php');
			}


		} else {
			// Nukreipimas i forma, 
			// klaidos spausdinimas per SESIJAS
			echo "<h1>Slaptažodžis ivestas neteisingai</h1>";
		}

	} else {
		header('Location: ../index.php');
	}