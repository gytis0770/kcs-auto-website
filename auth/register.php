<?php

	require('../db/connection.php');

	/* 
		Tikriname ar laukeliai formos uzpildyti
		ir vartotojas nebando patekti per adreso laukeli.
		Jei uzpildyti, gauname duomenis is formos,
		jei abu slaptazodziai sutampa,
		slaptazodis uzkoduojame su BCRYPT
		ir iterpiame i DB.
		Pagal nutylejima, kiekvienas naujas vartotojas
		tures tipa 0 - t.y. bus user tipo.
		Jei 1 - admin.
	*/
	if(!empty($_POST['username'])
		&& !empty($_POST['email'])
		&& !empty($_POST['password'])
		&& !empty($_POST['cpassword'])
		&& !empty($_POST['fullname'])
		&& !empty($_POST['city']))
	{
		// Gaunam duomenis is formos
		$username 	= $_POST['username'];
		$email 		= $_POST['email'];
		$password 	= $_POST['password'];
		$cpassword 	= $_POST['cpassword'];
		$fullname 	= $_POST['fullname'];
		$city 		= $_POST['city'];

		// Tikrinimas ar slaptazodziai sutampa
		if($password === $cpassword) {
			// Slaptazodzio kodavimas
			$password = password_hash($_POST['password'], PASSWORD_DEFAULT);

			// Vartotojo iterpimas i DB
			try {
				$sql = "
				INSERT INTO users 
				(username, email, password, fullname, city, type)
				VALUES
				('$username', '$email', '$password', '$fullname', '$city', 0)
				";

				if($conn->exec($sql)) {
					header('Location: loginView.php');
				}

			} catch(PDOException $e) {
				echo "Klaida: " . $e->getMessage();
			}

		} else {
			// Nukreipimas i forma, 
			// klaidos spausdinimas per SESIJAS
			echo "<h1>Slaptažodžiai nesutampa</h1>";
		}

	} else {
		header('Location: ../index.php');
	}